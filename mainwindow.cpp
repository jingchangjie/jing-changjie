#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QVBoxLayout"
#include "QHBoxLayout"
#include "QLabel"
#include "QPushButton"
#include "QDebug"
#include "qdebug.h"
#include <QMetaMethod>
#include <QObject>
#include "QTextBrowser"
#include "QStack"
#include "QVector"
#include "qchar.h"
#include "qchar.h"
#include "QChar"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    postfixStack.clear();
    infix.clear();
    operRes = 0;

    this->setWindowTitle("计算器");
    QIcon icon(":/new/prefix1/icon.png");
    this->setWindowIcon(icon);


    this->setStyleSheet(
                "QPushButton#ndeng{color: rgb(255, 255, 255);background-color:#ff8c00;}"
                "QPushButton#nc"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#nzuo"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#nyou"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#ndelete"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#nchu"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#njian"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#njia"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#ncheng"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#npercen"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"
                "QPushButton#npower"
                      "{color: rgb(255, 140, 0);background-color:#ffffff;}"

                );

    QVBoxLayout *v4 = new QVBoxLayout;
    v4->addWidget(ui->ndelete,1);
    v4->addWidget(ui->nchu,1);
    v4->addWidget(ui->njian,1);
    v4->addWidget(ui->njia,1);
    v4->addWidget(ui->ndeng,2);

    QHBoxLayout *h1 = new QHBoxLayout;
    h1->addWidget(ui->nc);
    h1->addWidget(ui->nzuo);
    h1->addWidget(ui->nyou);

    QHBoxLayout *h2 = new QHBoxLayout;
    h2->addWidget(ui->npercen);
    h2->addWidget(ui->npower);
    h2->addWidget(ui->ncheng);

    QHBoxLayout *h3 = new QHBoxLayout;
    h3->addWidget(ui->n7);
    h3->addWidget(ui->n8);
    h3->addWidget(ui->n9);

    QHBoxLayout *h4 = new QHBoxLayout;
    h4->addWidget(ui->n4);
    h4->addWidget(ui->n5);
    h4->addWidget(ui->n6);

    QHBoxLayout *h5 = new QHBoxLayout;
    h5->addWidget(ui->n1);
    h5->addWidget(ui->n2);
    h5->addWidget(ui->n3);

    QHBoxLayout *h6 = new QHBoxLayout;
    h6->addWidget(ui->n0,2);
    h6->addWidget(ui->ndian,1);

    QVBoxLayout *vl = new QVBoxLayout();
    vl->addLayout(h1,1);
    vl->addLayout(h2,1);
    vl->addLayout(h3,1);
    vl->addLayout(h4,1);
    vl->addLayout(h5,1);
    vl->addLayout(h6,1);

    QHBoxLayout *hd = new QHBoxLayout();
    hd->addLayout(vl,3);
    hd->addLayout(v4,1);

    QVBoxLayout *all = new QVBoxLayout(ui->centralwidget);
    all->addWidget(ui->textBrowser,1);
    all->addLayout(hd,5);

    QLabel *permanent = new QLabel(this);
    permanent->setFrameStyle(QFrame::Box | QFrame::Sunken);
    permanent->setText(
      tr("<a href=\"https://gitee.com/byzihao\">gitee.com/byzihao</a>"));
    permanent->setTextFormat(Qt::RichText);//文本将被解释为HTML
    permanent->setOpenExternalLinks(true);
    ui->statusbar->addPermanentWidget(permanent);
}

enum OperValue{
    NAME,NUMBER,END='#',PLUS='+',MINUS='-',MUL='*',DIV='/',PRINT=';',ASSIGN='=',LP='(',RP=')'
};

void MainWindow::on_ndeng_clicked()
{
    if(infix.isEmpty())
    {
        qDebug()<<"尚未输入表达式";
        return;
    }
    Topostfix();

    QStack <float> stack;
    float x1 ,x2 ;

    auto itr = postfixStack.begin();
    while(itr != postfixStack.end())
    {
        if(!IsOperator((*itr)[0]))
        {
            stack.push(itr->toFloat());
        }
        else
        {
            x2 = stack.pop();
            x1 = stack.pop();
            switch(itr->at(0).toLatin1())
            {
            case '+':
                stack.push(x1 + x2);break;
            case '-':
                stack.push(x1 - x2);break;
            case '*':
                stack.push(x1 * x2);break;
            case '/':
                stack.push(x1 / x2);break;
            case '%':
                if(x2 - (int)x2 == 0 && x1 - (int)x1 == 0)
                    stack.push((int)x1 % (int)x2);break;
            case '^':
                int power = 1;
                for (int i = 0; i < x2; ++i)
                {
                    power *= x1;
                }
                stack.push(power);
                break;
            }
        }
        ++itr;
    }

    operRes = stack.top();

    qDebug() << "运算结果:" << operRes;

    infix.append(QString::number(operRes));

    textBrowserShow();

}

void MainWindow::ReadSpace(
        QString::iterator &_itr,
        QString::iterator &_end)
{
    while(_itr != _end)
    {
        if (_itr->isSpace())
        {
            ++_itr;
        }
        else
        {
            break;
        }
    }
}


void MainWindow::ReadNumber(
        QString::iterator &_itr,
        QString::iterator &_end,
        QString &_value)
{
    while(_itr != _end)
    {
        if(_itr->isDigit() || _itr->toLatin1() == '.')
        {
            _value.push_back(*_itr++);
        }
        else
        {
            break;
        }
    }
}

int MainWindow::Priority(QChar &op)
{
    char opt = op.toLatin1();
    switch(opt)
    {
    case '#':
        return -1;
    case '(':
        return 0;
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
    case '%':
        return 2;
    case '^':
        return 3;
    default :
        return -1;
    }
}

bool MainWindow::IsOperator(QChar op)
{
    char opt = op.toLatin1();
    switch(opt)
    {
    case '#':
    case '(':
    case '+':
    case '-':
    case '*':
    case '/':
    case '%':
    case '^':
        return true;
    default :
        return false;
    }
}

int MainWindow::Topostfix()
{
    QStack<QChar> operatorStack;
    postfixStack.clear();
    operatorStack.push('#');

    auto preItr = infix.begin();
    auto end    = infix.end();

    QString strNumber;

    while(preItr != infix.end())
    {
        ReadSpace(preItr,end);
        strNumber.clear();
        if(preItr->isDigit())
        {
            ReadNumber(preItr,end,strNumber);
            postfixStack.push_back(strNumber);
        }
        else if(*preItr == '(')
        {
            operatorStack.push(*preItr);
            preItr++;
        }
        else if(*preItr == ')')
        {
            while (operatorStack.top()!='(')
            {
                postfixStack.push_back(QString(1,operatorStack.pop()));
            }
            operatorStack.pop();
            ++preItr;
        }
        else if(IsOperator(*preItr) )
        {
            while (Priority(*preItr) <= Priority(operatorStack.top()))
            {
                postfixStack.push_back(QString(1,operatorStack.pop()));
            }
            operatorStack.push(*preItr++);
        }
        else if(!preItr->isSpace())
        {
            return -1;
        }
    }

    while(!operatorStack.empty())
    {
        if(operatorStack.top() == '(')
        {
            return -1;
        }
        postfixStack.push_back(QString(1,operatorStack.pop()));
    }

    postfixStack.pop_back();

    infix.clear();
    qDebug()<<postfixStack;

    return 0;
}

void MainWindow::textBrowserShow()
{
    ui->textBrowser->setText(infix);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_ndelete_clicked()
{
    infix.remove(infix.size() -1,1);
    textBrowserShow();
}


void MainWindow::on_nc_clicked()
{
    infix.clear();
    textBrowserShow();
}

void MainWindow::on_njia_clicked()
{
    operatorInput('+');
    textBrowserShow();
}

void MainWindow::on_n7_clicked()
{
    infix.append(QString::number(7));
    textBrowserShow();
}

void MainWindow::on_n8_clicked()
{
    infix.append(QString::number(8));

    textBrowserShow();
}

void MainWindow::on_n9_clicked()
{
    infix.append(QString::number(9));
    textBrowserShow();
}

void MainWindow::operatorInput(char sym)
{
    if(infix.isEmpty())
    {
        infix.append(QString::number(0));
    }
    if(!IsOperator((infix.end()-1)->toLatin1()))
    {
        infix.append(sym);
    }
}

void MainWindow::on_njian_clicked()
{

    operatorInput('-');
    textBrowserShow();
}

void MainWindow::on_n4_clicked()
{
    infix.append(QString::number(4));
    textBrowserShow();
}

void MainWindow::on_n5_clicked()
{
    infix.append(QString::number(5));
    textBrowserShow();
}

void MainWindow::on_n6_clicked()
{
    infix.append(QString::number(6));
    textBrowserShow();
}

void MainWindow::on_ncheng_clicked()
{
    operatorInput('*');
    textBrowserShow();
}

void MainWindow::on_n1_clicked()
{
    infix.append(QString::number(1));
    textBrowserShow();
}

void MainWindow::on_n2_clicked()
{
    infix.append(QString::number(2));
    textBrowserShow();
}

void MainWindow::on_n3_clicked()
{
    infix.append(QString::number(3));
    textBrowserShow();
}

void MainWindow::on_nchu_clicked()
{
    operatorInput('/');
    textBrowserShow();
}

void MainWindow::on_n0_clicked()
{
    infix.append(QString::number(0));
    textBrowserShow();
}

void MainWindow::on_ndian_clicked()
{
    auto pos = infix.end() -1;
    for(;!IsOperator(pos->toLatin1()) && pos != infix.begin();pos --)
    {
        if(pos->toLatin1() == '.')
            return;

    }
    if( pos->toLatin1() == '%')
    {

        return;
    }
    infix.append('.');
    textBrowserShow();
}


void MainWindow::on_nzuo_clicked()
{
    infix.append('(');
    textBrowserShow();
}

int MainWindow::countSym(const QString &input,const char &sym)
{
    int count = 0;
    auto pos = input.begin();
    while(1)
    {
        auto res = std::find(pos,input.end(),sym);
        if(res != input.end())
        {
            count ++;
            pos = res + 1;
        }
        else
            break;
    }
    return count;
}

void MainWindow::on_nyou_clicked()
{
    if(countSym(infix,'(') > countSym(infix,')'))
        infix.append(')');
    textBrowserShow();
}

void MainWindow::on_npercen_clicked()
{
    QString value;

    auto _itr = infix.end() -1;
    while(_itr != infix.begin())
    {
        if(_itr->isDigit() || _itr->toLatin1() == '.')
        {

            value.push_back(*_itr--);
        }
        else
        {
            break;
        }
    }

    if(countSym(value,'.')== 0)
    {
        operatorInput('%');
        textBrowserShow();
    }

}

void MainWindow::on_npower_clicked()
{

}
