#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QStack"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ndelete_clicked();

    void on_nc_clicked();

    void on_njia_clicked();

    void on_n7_clicked();

    void on_n8_clicked();

    void on_n9_clicked();

    void on_njian_clicked();

    void on_n4_clicked();

    void on_n5_clicked();

    void on_n6_clicked();

    void on_ncheng_clicked();

    void on_n1_clicked();

    void on_n2_clicked();

    void on_n3_clicked();

    void on_nchu_clicked();

    void on_n0_clicked();

    void on_ndian_clicked();

    void on_ndeng_clicked();

    void on_nzuo_clicked();

    void on_nyou_clicked();

    void on_npercen_clicked();

    void on_npower_clicked();

private:
    void textBrowserShow();
    int  Topostfix();
    void ReadSpace(
            QString::iterator & _itr,
            QString::iterator & _end);

    void ReadNumber(
            QString::iterator &_itr,
            QString::iterator &_end,
            QString &_value);

    int countSym(const QString &input,const char &sym);
    bool IsOperator(QChar op);
    int Priority(QChar &op);
    void operatorInput(char sym);

    Ui::MainWindow *ui;
    QStack <QString> postfixStack;
    QString infix;
    float operRes;
};
#endif // MAINWINDOW_H
